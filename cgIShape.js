//
// fill in code that creates the triangles for a cube with dimensions 1x1x1
// on each side (and the origin in the center of the cube). with an equal
// number of subdivisions along each cube face as given by the parameter
//subdivisions
//

function makeCube (subdivisions)
{

    if( subdivisions < 1 )
        subdivisions = 1;

	steps = 1 / subdivisions;
	
	/*
	for a given face, we consider the following points:
	(u0,v1) +---+ (u1,v1)
			|   |
	(u0,v0) +---+ (u1,v0)
	*/

	for (var i = 0; i < subdivisions; i++) {	
		var u0 = i * steps - 0.5;
		var u1 = (i + 1) * steps - 0.5;
		for (var j = 0; j < subdivisions; j++) {
			var v0 = j * steps - 0.5;
			var v1 = (j + 1) * steps - 0.5;
			// face (x,y,-.5) drawn clockwise
			addTriangle(u1, v0, -0.5, u0, v0, -0.5, u0, v1, -0.5);
			addTriangle(u1, v0, -0.5, u0, v1, -0.5, u1, v1, -0.5);
			// face (x,y,.5) drawn counterclockwise
			addTriangle(u0, v0, 0.5, u1, v0, 0.5, u0, v1, 0.5);
			addTriangle(u0, v1, 0.5, u1, v0, 0.5, u1, v1, 0.5);
			// face (-.5,y,z) drawn counterclockwise
			addTriangle(-0.5, u0, v1, -0.5, u1, v0, -0.5, u0, v0);
			addTriangle(-0.5, u1, v1, -0.5, u1, v0, -0.5, u0, v1);
			// face (.5,y,z) drawn clockwise
			addTriangle(0.5, u1, v0, 0.5, u0, v1, 0.5, u0, v0);
			addTriangle(0.5, u1, v0, 0.5, u1, v1, 0.5, u0, v1);
			// face (x,-.5,z) drawn clockwise
			addTriangle(u1, -0.5, v0, u0, -0.5, v1, u0, -0.5, v0);
			addTriangle(u1, -0.5, v0, u1, -0.5, v1, u0, -0.5, v1);
			// face (x,.5,z) drawn counterclockwise
			addTriangle(u0, 0.5, v1, u1, 0.5, v0, u0, 0.5, v0);
			addTriangle(u1, 0.5, v1, u1, 0.5, v0, u0, 0.5, v1);
		}
	}
}





// //
// // fill in code that creates the triangles for a cylinder with diameter 1
// // and height of 1 (centered at the origin) with the number of subdivisions
// // around the base and top of the cylinder (given by radialdivision) and
// // the number of subdivisions along the surface of the cylinder given by
// //heightdivision.
// //
function makeCylinder (radialDivisions,heightDivisions){
    // fill in your code here.
    var radius = 1;
    if(radialDivisions < 3 )
        radialDivisions = 3;

    if( heightDivisions < 1 )
        heightDivisions = 1;

	const PI = Math.PI;
    let y0 = -0.5, y1,
		x0, z0, x1,	z1;
	for (var i = 0; i < radialDivisions; i++) {
		
		x0 = radius * Math.cos(i * 2 * PI / radialDivisions);
		z0 = radius * Math.sin(i * 2 * PI / radialDivisions);
		x1 = radius * Math.cos((i+1) * 2 * PI / radialDivisions);
		z1 = radius * Math.sin((i+1) * 2 * PI / radialDivisions);

		// face (x,-.5,z) drawn clockwise
		addTriangle(0, -0.5, 0, x0, -0.5, z0, x1, -0.5, z1);
		// face (x,5,z) drawn counterclockwise
		addTriangle(x1, 0.5, z1, x0, 0.5, z0, 0, 0.5, 0);

		// draw the rectangles for the height
		for (var j = 0; j < heightDivisions; j++) {
			y0 = (j) / heightDivisions - 0.5;
			y1 = (j + 1) / heightDivisions - 0.5;
			addTriangle(x0, y1, z0, x1, y1, z1, x0, y0, z0);
			addTriangle(x1, y1, z1, x1, y0, z1, x0, y0, z0);
		}
	}
}


 //
// // fill in code that creates the triangles for a cone with diameter 1
// // and height of 1 (centered at the origin) with the number of
// // subdivisions around the base of the cone (given by radialdivision)
// // and the number of subdivisions along the surface of the cone
// //given by heightdivision.
// //
function makeCone (radialdivision, heightdivision) {


    var radius = 1;
    var radialDivisions = radialdivision;
    var heightDivisions = heightdivision;
    if( radialDivisions < 3 )
        radialDivisions = 3;

    if( heightDivisions < 1 )
        heightDivisions = 1;

	var PI = Math.PI;

	for (var i = 0; i < radialDivisions; i++) {
		
		var x0 = radius * Math.cos(i * 2 * PI / radialDivisions);
		var z0 = radius * Math.sin(i * 2 * PI / radialDivisions);
		 var x1 = radius * Math.cos((i + 1) * 2 * PI / radialDivisions);
		 var z1 = radius * Math.sin((i + 1) * 2 * PI / radialDivisions);

		// face (x,5,z) drawn counterclockwise
		addTriangle(x0, -0.5, z0, x1, -0.5, z1, 0.0, -0.5, 0.0);

		var y0 = -0.5;
		var cx0 = -x0 / heightDivisions;
		var cz0 = -z0 / heightDivisions;
		 var cx1 = -x1 / heightDivisions;
		 var cz1 = -z1 / heightDivisions;
		var y1 = 1.0 / heightDivisions;
		// draw the rectangles for the height
		for (var j = 0; j < heightDivisions - 1; j++) {
		// else we wanna draw trapezium			
			addTriangle(x0, y0, z0, x0+cx0, y0+y1, z0+cz0, x1, y0, z1);
			addTriangle(x0+cx0, y0+y1, z0+cz0, x1+cx1, y0+y1, z1+cz1, x1, y0, z1);

			x0 += cx0;
			z0 += cz0;
			x1 += cx1;
			z1 += cz1;
			y0 += y1;
		}

		// when we are at the top of the cone, just draw a triangle
		addTriangle(x0, y0, z0, 0.0, 0.5, 0.0, x1, y0, z1);
	}
}

/ //
// // fill in code that creates the triangles for a sphere with diameter 1
// // (centered at the origin) with number of slides (longitude) given by
// // slices and the number of stacks (lattitude) given by stacks.
// // For this function, you will implement the tessellation method based
// // on spherical coordinates as described in the video (as opposed to the
// //recursive subdivision method).
// //


function makeSphere(slices,stacks)
{
	// IF USING RECURSIVE SUBDIVISION, MODIFY THIS TO USE
	// A MINIMUM OF 1 AND A MAXIMUM OF 5 FOR 'slices'


    var radius = 1;
	if (slices < 1)
		slices = 1;
	if (slices > 5)
		slices = 5;

	if (stacks < 3)
		stacks = 3;

	
	// var a = radius;
	//declaration of the 19 triangles of the icosahedron
	//Triangle0 = <V0,V1,V2>
	recursiveTriangle(0, radius, -1, -radius, 1, 0, radius, 1, 0, slices);
	//Triangle1 = <V3, V2, V1>
	recursiveTriangle(0, radius, 1, radius, 1, 0, -radius, 1, 0, slices);
	//Triangle2 = <V3, V4, V5>
	recursiveTriangle(0, radius, 1, -1, 0, radius, 0, -radius, 1, slices);
	//Triangle3 = <V3, V5, V6>
	recursiveTriangle(0, radius, 1, 0, -radius, 1, 1, 0, radius, slices);
	//Triangle4 = <V0, V7, V8>
	recursiveTriangle(0, radius, -1, 1, 0, -radius, 0, -radius, -1, slices);
	//Triangle5 = <V0, V8, V9>
	recursiveTriangle(0, radius, -1, 0, -radius, -1, -1, 0, -radius, slices);
	//Triangle6 = <V5, V10, V11>
	recursiveTriangle(0, -radius, 1, -radius, -1, 0, radius, -1, 0, slices);
	//Triangle7 = <V8, V11, V10>
	recursiveTriangle(0, -radius, -1, radius, -1, 0, -radius, -1, 0, slices);
	//Triangle8 = <V1, V9, V4>
	recursiveTriangle(-radius, 1, 0, -1, 0, -radius, -1, 0, radius, slices);
	//Triangle9 = <V10, V4, V9>
	recursiveTriangle(-radius, -1, 0, -1, 0, radius, -1, 0, -radius, slices);
	//Triangle10 = <V2, V6, V7>
	recursiveTriangle(radius, 1, 0, 1, 0, radius, 1, 0, -radius, slices);
	//Triangle11 = <V11, V7, V6>
	recursiveTriangle(radius, -1, 0, 1, 0, -radius, 1, 0, radius, slices);
	//Triangle12 = <V3, V1, V4>
	recursiveTriangle(0, radius, 1, -radius, 1, 0, -1, 0, radius, slices);
	//Triangle13 = <V3, V6, V2>
	recursiveTriangle(0, radius, 1, 1, 0, radius, radius, 1, 0, slices);
	//Triangle14 = <V0, V9, V1>
	recursiveTriangle(0, radius, -1, -1, 0, -radius, -radius, 1, 0, slices);
	//Triangle15 = <V0, V2, V7>
	recursiveTriangle(0, radius, -1, radius, 1, 0, 1, 0, -radius, slices);
	//Triangle16 = <V8, V10, V9>
	recursiveTriangle(0, -radius, -1, -radius, -1, 0, -1, 0, -radius, slices);
	//Triangle17 = <V8, V7, V11>
	recursiveTriangle(0, -radius, -1, 1, 0, -radius, radius, -1, 0, slices);
	//Triangle18 = <V5, V4, V10>
	recursiveTriangle(0, -radius, 1, -1, 0, radius, -radius , -1, 0, slices);
	//Triangle19 = <V5, V11, V6>
	recursiveTriangle(0, -radius, 1, radius, -1, 0, 1, 0, radius, slices);
}

function recursiveTriangle(x0, y0, z0,x1,y1,z1,x2,y2,z2,subdivision)
{
    var radius  = 1;
	if (subdivision == 1)
	{
		//Normalization of the point 0
		var norm0 = Math.pow((Math.pow(x0, 2) + Math.pow(y0, 2) + Math.pow(z0, 2)), 0.5);
		x0 = (x0 / norm0) * radius;
		y0 = (y0 / norm0) * radius;
		z0 = (z0 / norm0) * radius;
		//Normalization of the point 1
		var norm1 = Math.pow((Math.pow(x1, 2) + Math.pow(y1, 2) + Math.pow(z1, 2)), 0.5);
		x1 = (x1 / norm1) * radius;
		y1 = (y1 / norm1) * radius;
		z1 = (z1 / norm1) * radius;
		//Normalization of the point 2
		var norm2 = Math.pow((Math.pow(x2, 2) + Math.pow(y2, 2) + Math.pow(z2, 2)), 0.5);
		x2 = (x2 / norm2) * radius;
		y2 = (y2 / norm2) * radius;
		z2 = (z2 / norm2) * radius;
		addTriangle(x0, y0, z0, x1, y1, z1,x2, y2, z2);
	}
	else
	{
		//Calculate the point on the middle of the edge point0 - point1
		var midx01 = (x0 + x1) / 2;
		var midy01 = (y0 + y1) / 2;
		var midz01 = (z0 + z1) / 2;

		//Calculate the point on the middle of the edge point1 - point2
		var midx12 = (x1 + x2) / 2;
		var midy12 = (y1 + y2) / 2;
		var midz12 = (z1 + z2) / 2;

		//Calculate the point on the middle of the edge point2 - point0
		var midx20 = (x0 + x2) / 2;
		var midy20 = (y0 + y2) / 2;
		var midz20 = (z0 + z2) / 2;

		//Call 4 times (1 call per sub triangles created) the recursiveTriangle method with subdivision-1,
		recursiveTriangle(x0, y0, z0, midx01, midy01, midz01, midx20, midy20, midz20, (subdivision - 1));
		recursiveTriangle(midx01, midy01, midz01, midx12, midy12, midz12, midx20, midy20, midz20, (subdivision - 1));
		recursiveTriangle(midx01, midy01, midz01, x1, y1, z1, midx12, midy12, midz12, (subdivision - 1));
		recursiveTriangle(midx20, midy20, midz20, midx12, midy12, midz12, x2, y2, z2, (subdivision - 1));
	}
}









////////////////////////////////////////////////////////////////////
//
//  Do not edit below this line
//
///////////////////////////////////////////////////////////////////

function radians(degrees)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}

function addTriangle (x0,y0,z0,x1,y1,z1,x2,y2,z2) {

    
    var nverts = points.length / 4;
    
    // push first vertex
    points.push(x0);  bary.push (1.0);
    points.push(y0);  bary.push (0.0);
    points.push(z0);  bary.push (0.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++;
    
    // push second vertex
    points.push(x1); bary.push (0.0);
    points.push(y1); bary.push (1.0);
    points.push(z1); bary.push (0.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++
    
    // push third vertex
    points.push(x2); bary.push (0.0);
    points.push(y2); bary.push (0.0);
    points.push(z2); bary.push (1.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++;
}

